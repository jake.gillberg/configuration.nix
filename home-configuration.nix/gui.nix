{ pkgs, lib, ... }:
with pkgs;
lib.mkMerge [
  ( import ./kitty.nix )
  ( import ./monoid.nix { inherit nerdfonts; } )
]
