{ vimPlugins }: {

  home.sessionVariables = {
    VISUAL = "nvim";
  };

  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    plugins = with vimPlugins; [
      gruvbox
      vim-airline
      vim-devicons
      vim-nix
      vim-hcl
    ];
    extraConfig = ''
      set termguicolors
      let g:gruvbox_italic = 1
      autocmd vimenter * colorscheme gruvbox

      let g:airline_powerline_fonts = 1
      let g:airline#extensions#tabline#enabled = 1
      let g:airline#extensions#tabline#buffer_min_count = 2
      autocmd vimenter * AirlineTheme gruvbox
      
      set hidden
    '';
  };
}
