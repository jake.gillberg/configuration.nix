{ nerdfonts }: {
  fonts.fontconfig.enable = true;
  home.packages = [
    (nerdfonts.override { fonts = [ "Monoid" ]; })
  ];
}
