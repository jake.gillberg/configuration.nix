{
  programs.kitty = {
    enable = true;
    font = {
      name = "Monoid Nerd Font Retina";
      size = 15;
    };
    theme = "Gruvbox Dark";
  };
}
