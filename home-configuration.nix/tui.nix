{ pkgs, lib, ... }:
with pkgs;
lib.mkMerge [
  { home.packages = [
      awscli2
      gnupg
      openssl
      poetry
      python39
      terraform
      unzip
    ];
  }
  ( import ./fish.nix {inherit fetchFromGitHub;} )
  ( import ./vim.nix {inherit vimPlugins;} )
  ( import ./direnv.nix )
  ( import ./git.nix )
  ( import ./gpg.nix )
]
