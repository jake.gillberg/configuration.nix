{ pkgs, lib, stdenv, configuration ? null, ... }:
let
  gui = lib.elem configuration [null "rigour"];
in
lib.mkMerge [
  {
    home = {
      stateVersion = "23.11";
      shellAliases = {
        nixos-rebuild = if stdenv.isDarwin then "darwin-rebuild" else "nixos-rebuild";
      };
    };
  }
  (pkgs.callPackage ./tui.nix)
  (lib.mkIf gui (pkgs.callPackage ./gui.nix))
]
