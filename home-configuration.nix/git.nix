{
  programs.git = {
    enable = true;
    ignores = [ "*~" ".swp" "result" ".env*" ".direnv" ];
    userEmail = "jgillberg@galileo.io";
    userName = "Jake Gillberg";
    extraConfig = {
      init = { defaultBranch = "main"; };
      push = { autoSetupRemote = true; };
      pull = { rebase = true; };
    };
    signing = {
      key = null;
      signByDefault = true;
    };
  };

  programs.git-credential-oauth.enable = true;
}
