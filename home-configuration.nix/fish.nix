{ fetchFromGitHub }: {
  programs.fish = {
    enable = true;
    shellAbbrs = {
      clean = "sudo nix-collect-garbage -d";
      update = "nix flake update ~/configuration.nix/#";
      build = "nixos-rebuild switch --flake ~/configuration.nix/#";
    };
    interactiveShellInit = ''
      # for nix-darwin
      fenv source /etc/static/bashrc;

      # gpg
      set -gx GPG_TTY (tty)
      
      set -g theme_nerd_fonts yes
      set -g theme_color_scheme terminal
    '';
    plugins = [
      { name = "foreign-env";
	src = fetchFromGitHub {
	  owner = "oh-my-fish";
	  repo = "plugin-foreign-env";
	  rev = "7f0cf099ae1e1e4ab38f46350ed6757d54471de7";
	  hash = "sha256-4+k5rSoxkTtYFh/lEjhRkVYa2S4KEzJ/IJbyJl+rJjQ=";
	};
      }
      { name = "bobthefish";
        src = fetchFromGitHub {
          owner = "oh-my-fish";
          repo = "theme-bobthefish";
          rev = "c2c47dc964a257131b3df2a127c2631b4760f3ec";
          hash = "sha256-LB4g+EA3C7OxTuHfcxfgl8IVBe5NufFc+5z9VcS0Bt0=";
        };
      }
    ];
  };

  programs.kitty.settings = {
    shell = "/etc/profiles/per-user/jgillberg/bin/fish";
  };
}
