{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    nix-darwin = {
      url = "github:LnL7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, home-manager, nix-darwin, nixpkgs }:
  let
    configurationRevision = self.rev or self.dirtyRev or null;
  in
  {
    # Build darwin flake using:
    # $ darwin-rebuild build --flake .#rigour
    darwinConfigurations.rigour = nix-darwin.lib.darwinSystem {
      specialArgs = {
        inherit configurationRevision;
        configuration = "rigour";
      };
      modules = [
	home-manager.darwinModules.home-manager
        (import ./darwin-configuration.nix)
        {
          nixpkgs.hostPlatform = "aarch64-darwin";
          nixpkgs.config.allowUnfree = true;
	}
      ];
    };

    # Expose the package set, including overlays, for convenience.
    darwinPackages = self.darwinConfigurations.rigour.pkgs;
  };
}
