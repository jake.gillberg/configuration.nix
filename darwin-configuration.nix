{ pkgs, lib, configuration, configurationRevision, ... }:
let
  gruvbox = import ./home-configuration.nix/gruvbox.nix;
in
{

  nix = {
    package = pkgs.nixFlakes;
    settings.experimental-features = "nix-command flakes";
  };

  services.nix-daemon.enable = true;

  system = {
    configurationRevision = configurationRevision;

    # Used for backwards compatibility, please read the changelog before changing.
    # $ darwin-rebuild changelog
    stateVersion = 4;
  };

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;

    users.jgillberg = pkgs.callPackage ./home-configuration.nix/home-configuration.nix {};
  };

  users.users.jgillberg = {
    home = /Users/jgillberg;
    shell = pkgs.fish;
  };
}
